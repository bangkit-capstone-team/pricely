package com.bangkit.pricely.util

import com.bangkit.pricely.BuildConfig

object AppConst{
    const val API_URL_BASE = BuildConfig.API_BASE_URL
}
